import pygame
import main as game
from difficulty import *
from colors import *
from button import *
pygame.init()

def redrawWindow():
    screen.fill(backgroundColor)
    button1.draw(screen)
    button2.draw(screen)
    button3.draw(screen)

def main():
    pygame.display.set_caption('Minesweeper')

    global screen
    screen = pygame.display.set_mode((400, 500))

    global button1
    button1 = Button(100, 300, 50, 50, "Easy")

    global button2
    button2 = Button(100, 300, 50, 190, "Advanced")

    global button3
    button3 = Button(100, 300, 50, 340, "Expert")

    global buttons
    buttons = [button1, button2, button3]

    running = True
    while running:
        pygame.display.update()
        redrawWindow()

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                quit()

            if event.type == pygame.MOUSEMOTION:
                mousePos = pygame.mouse.get_pos()
                for i in buttons:
                    if i.isOver(mousePos):
                        i.color = tileClickedColor
                    else:
                        i.color = tileColor

            if event.type == pygame.MOUSEBUTTONDOWN:
                mousePos = pygame.mouse.get_pos()
                if event.button == 1:
                    for i in buttons:
                        if i.isOver(mousePos):
                            if i == button1:
                                game.main(easyMode)
                            elif i == button2:
                                game.main(advancedMode)
                            elif i == button3:
                                game.main(expertMode)
                            else:
                                print("Debug.Log('somethings go wrong')")

        pygame.display.flip()

if __name__ == "__main__":
    main()