import pygame
from colors import *

class Button():
    def __init__(self, height, width, xCor, yCor, text):
        self.height = height
        self.width = width
        self.x = xCor
        self.y = yCor
        self.text = text
        self.color = tileColor
    def draw(self, screen):
        pygame.draw.rect(screen, blackColor, (self.x - 1, self.y - 1, self.width + 2, self.height + 2), 0)
        pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height), 0)
        font = pygame.font.SysFont('comicsansms', 50)
        text = font.render(self.text, 1, blackColor)
        screen.blit(text, (
            self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))
    def isOver(self, pos):
        if pos[0] > self.x and pos[0] < self.x + self.width:
            if pos[1] > self.y and pos[1] < self.y + self.height:
                return True
        return False
    def main(self):
        pass

if __name__ == "__main__":
    Button.main()