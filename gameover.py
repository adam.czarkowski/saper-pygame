import pygame
from colors import *

class GameOverScreen():
    def __init__(self, screen, difficulty):
        self.difficulty = difficulty
        self.screen = screen
        self.width = 300
        self.height = 100
        self.x = (difficulty.widthScreen - self.width) / 2
        self.y = 25
        self.gameStatus = 0
        self.color = tileClickedColor
    def draw(self):
        if self.gameStatus > 0:
            pygame.draw.rect(self.screen, blackColor, (self.x - 1, self.y - 1, self.width + 2, self.height + 2), 0)
            pygame.draw.rect(self.screen, self.color, (self.x, self.y, self.width, self.height), 0)
        if self.gameStatus == 0:
            font = pygame.font.SysFont('comicsansms', 60)
            text = font.render("SAPER", 1, blackColor)
            self.screen.blit(text, (
            self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))
        if self.gameStatus == 1:
            font = pygame.font.SysFont('comicsansms', 60)
            text = font.render("LOSE", 1, redColor)
            self.screen.blit(text, (
            self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))
        if self.gameStatus == 2:
            font = pygame.font.SysFont('comicsansms', 60)
            text = font.render("WIN", 1, greenColor)
            self.screen.blit(text, (
            self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))
    def isOver(self, pos):
        if pos[0] > self.x and pos[0] < self.x + self.width:
            if pos[1] > self.y and pos[1] < self.y + self.height:
                return True
        return False
    def main(self):
        pass

if __name__ == "__main__":
    GameOverScreen.main()