import pygame

class Tile():
    def __init__(self, color, x, y, width, height, text=''):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.isMine = False
        self.isEmpty = True
        self.isDiscovered = False
        self.isFlagged = False
        self.number = "0"
    def draw(self, win, outline=None):
        if outline:
            pygame.draw.rect(win, outline, (self.x - 1, self.y - 1, self.width + 2, self.height + 2), 0)
        pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.height), 0)
        if self.text != '':
            font = pygame.font.SysFont('comicsansms', 30)
            text = font.render(self.text, 1, (0, 0, 0))
            win.blit(text, (
            self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))
    def isOver(self, pos):
        if pos[0] > self.x and pos[0] < self.x + self.width:
            if pos[1] > self.y and pos[1] < self.y + self.height:
                return True
        return False
    def main(self):
        pass

if __name__ == "__main__":
    Tile.main()