import pygame

class Difficulty():
    def __init__(self, widthScreen, heightScreen, widthBoard, heightBoard, xCor, yCor, minesAmount):
        self.widthScreen = widthScreen
        self.heightScreen = heightScreen
        self.widthBoard = widthBoard
        self.heightBoard = heightBoard
        self.xCor = xCor
        self.yCor = yCor
        self.minesAmount = minesAmount
    def main(self):
        pass

easyMode = Difficulty(400, 500, 8, 8, 40, 150, 10)
advancedMode = Difficulty(720, 820, 16, 16, 40, 150, 40)
expertMode = Difficulty(1360, 820, 32, 16, 40, 150, 99)

if __name__ == "__main__":
    Difficulty.main()
