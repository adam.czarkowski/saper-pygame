import pygame
from tile import *
from colors import *
from border import *
from difficulty import *
from gameover import *
import start
from random import randint
pygame.init()

def redrawWindow():
    screen.fill(backgroundColor)
    for row in tilemap:
        for tile in row:
           tile.draw(screen, blackColor)
    gameOverScreen.draw()

def createScreen(difficulty):
    screen = pygame.display.set_mode((difficulty.widthScreen, difficulty.heightScreen))
    return screen

def createTilemap(difficulty):
    simpleButtons = [[i for i in range(difficulty.widthBoard)] for i in range(difficulty.heightBoard)]
    for i in range(difficulty.heightBoard):
        for j in range(difficulty.widthBoard):
            simpleButtons[i][j] = Tile(tileColor, difficulty.xCor + (40 * j), difficulty.yCor + (40 * i), 40, 40)
    return simpleButtons

def createMines(tilemap, difficulty):
    for i in range(difficulty.minesAmount):
        randomRow = randint(0, difficulty.heightBoard - 1)
        randomTile = randint(0, difficulty.widthBoard - 1)
        while True:
            if (tilemap[randomRow][randomTile].isMine == True):
                randomRow = randint(0, difficulty.heightBoard - 1)
                randomTile = randint(0, difficulty.widthBoard - 1)
            else:
                tilemap[randomRow][randomTile].isMine = True
                tilemap[randomRow][randomTile].isEmpty = False
                tilemap[randomRow][randomTile].number = "X"
                break
    return tilemap

def createBorder(tilemap):
    for row in tilemap:
        row.reverse()
        row.append(Border())
        row.reverse()
        row.append(Border())
        lenght = len(row)
    tilemap.reverse()
    tilemap.append([Border() for i in range(lenght)])
    tilemap.reverse()
    tilemap.append([Border() for i in range(lenght)])
    return tilemap

def createValues(tilemap):
    for i in range(1, len(tilemap)-1):
        for j in range(len(tilemap[i])):
            if tilemap[i][j].isEmpty:
                counterMines = 0
                if (tilemap[i][j + 1].isMine): counterMines += 1
                if (tilemap[i][j - 1].isMine): counterMines += 1
                if (tilemap[i - 1][j].isMine): counterMines += 1
                if (tilemap[i + 1][j].isMine): counterMines += 1
                if (tilemap[i + 1][j + 1].isMine): counterMines += 1
                if (tilemap[i + 1][j - 1].isMine): counterMines += 1
                if (tilemap[i - 1][j + 1].isMine): counterMines += 1
                if (tilemap[i - 1][j - 1].isMine): counterMines += 1
                tilemap[i][j].number = str(counterMines)
                #tilemap[i][j].text = tilemap[i][j].number
                if (tilemap[i][j].number != "0"): tilemap[i][j].isEmpty = False
    return tilemap

def reccursionDiscover(tilemap, i, j):
    tilemap[i][j].isDiscovered = True
    tilemap[i][j].color = backgroundColor

    if (tilemap[i][j + 1].isEmpty) and not (tilemap[i][j + 1].isDiscovered): reccursionDiscover(tilemap, i, j + 1)
    elif (int(tilemap[i][j + 1].number) > 0):
        tilemap[i][j + 1].text = tilemap[i][j + 1].number
        tilemap[i][j + 1].color = backgroundColor
        tilemap[i][j + 1].isDiscovered = True

    if (tilemap[i][j - 1].isEmpty) and not (tilemap[i][j - 1].isDiscovered): reccursionDiscover(tilemap, i, j - 1)
    elif (int(tilemap[i][j - 1].number) > 0):
        tilemap[i][j - 1].text = tilemap[i][j - 1].number
        tilemap[i][j - 1].color = backgroundColor
        tilemap[i][j - 1].isDiscovered = True

    if (tilemap[i - 1][j].isEmpty) and not (tilemap[i - 1][j].isDiscovered): reccursionDiscover(tilemap, i - 1, j)
    elif (int(tilemap[i - 1][j].number) > 0):
        tilemap[i - 1][j].text = tilemap[i - 1][j].number
        tilemap[i - 1][j].color = backgroundColor
        tilemap[i - 1][j].isDiscovered = True

    if (tilemap[i + 1][j].isEmpty) and not (tilemap[i + 1][j].isDiscovered): reccursionDiscover(tilemap, i + 1, j)
    elif (int(tilemap[i + 1][j].number)>0):
        tilemap[i + 1][j].text = tilemap[i + 1][j].number
        tilemap[i + 1][j].color = backgroundColor
        tilemap[i + 1][j].isDiscovered = True

    if (int(tilemap[i + 1][j + 1].number) > 0):
        tilemap[i + 1][j + 1].text = tilemap[i + 1][j + 1].number
        tilemap[i + 1][j + 1].color = backgroundColor
        tilemap[i + 1][j + 1].isDiscovered = True

    if (int(tilemap[i + 1][j - 1].number) > 0):
        tilemap[i + 1][j - 1].text = tilemap[i + 1][j - 1].number
        tilemap[i + 1][j - 1].color = backgroundColor
        tilemap[i + 1][j - 1].isDiscovered = True

    if (int(tilemap[i - 1][j + 1].number) > 0):
        tilemap[i - 1][j + 1].text = tilemap[i - 1][j + 1].number
        tilemap[i - 1][j + 1].color = backgroundColor
        tilemap[i - 1][j + 1].isDiscovered = True

    if (int(tilemap[i - 1][j - 1].number) > 0):
        tilemap[i - 1][j - 1].text = tilemap[i - 1][j - 1].number
        tilemap[i - 1][j - 1].color = backgroundColor
        tilemap[i - 1][j - 1].isDiscovered = True

def main(mode):
    global screen
    global tilemap
    global gameOverScreen
    pygame.display.set_caption('Minesweeper')
    screen = createScreen(mode)
    tilemap = createTilemap(mode)
    tilemap = createMines(tilemap, mode)
    tilemap = createBorder(tilemap)
    tilemap = createValues(tilemap)
    clock = pygame.time.Clock()
    gameOverScreen = GameOverScreen(screen, mode)

    running = True
    redrawWindow()
    gameOver = False
    while running:
        pygame.display.update()
        redrawWindow()

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                mousePos = pygame.mouse.get_pos()

                if gameOverScreen.isOver(mousePos) and gameOverScreen.gameStatus > 0:
                    start.main()

                for i in range(1, len(tilemap) - 1):
                    for j in range(len(tilemap[i])):
                        if tilemap[i][j].isOver(mousePos) and not gameOver:
                            if event.button == 1:
                                if tilemap[i][j].isMine:
                                    tilemap[i][j].text = tilemap[i][j].number
                                    tilemap[i][j].color = redColor
                                    gameOver = True
                                    gameOverScreen.gameStatus = 1
                                elif int(tilemap[i][j].number) > 0:
                                    tilemap[i][j].text = tilemap[i][j].number
                                    tilemap[i][j].color = backgroundColor
                                elif tilemap[i][j].isEmpty:
                                    reccursionDiscover(tilemap, i, j)
                            if event.button == 3:
                                if not tilemap[i][j].isDiscovered:
                                    tilemap[i][j].isFlagged = not tilemap[i][j].isFlagged
                                    if tilemap[i][j].isFlagged:
                                        tilemap[i][j].text = "F"
                                    elif not tilemap[i][j].isFlagged:
                                        tilemap[i][j].text = ""

            if event.type == pygame.MOUSEMOTION:
                if gameOverScreen.gameStatus > 0:
                    mousePos = pygame.mouse.get_pos()
                    if gameOverScreen.gameStatus > 0:
                        if gameOverScreen.isOver(mousePos):
                            gameOverScreen.color = tileColor
                        if not gameOverScreen.isOver(mousePos):
                            gameOverScreen.color = tileClickedColor


        pygame.display.flip()
        clock.tick(60)

if __name__=="__main__":
    main()